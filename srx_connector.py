#!/usr/bin/env python
#       File name: srx_connector.py
#       Author: Jakub Toczek (PSNC)
#       Date created: 30/10/2017
#       Python Version: 2.7.13
from warden_client import Client, read_cfg
import datetime
from time import time
import sys
import os
import uuid
import subprocess

DEFAULT_ACONFIG = 'warden_client-srx.cfg'
DEFAULT_WCONFIG = 'warden_client.cfg'
DEFAULT_NAME = 'org.example.warden.test'
DEFAULT_AWIN = 5
DEFAULT_ANONYMISED = 'no'
DEFAULT_ANONYMISED_NET = '0.0.0.0/0'
DEFAULT_SECRET = ''

logfile = "/var/log/srx.log"
last_run_file = "last_run.log"

def write_common_information(dictionary,
                             event_id,
                             detect_time,
                             category = None,
                             description = None,
                             bytes_number = None,
                             packages = None):
    dictionary.update({'Format': 'IDEA0'})
    dictionary.update({'ID': event_id})
    dictionary.update({'DetectTime': detect_time})
    dictionary.update({'CreateTime': str(datetime.datetime.now().strftime('%Y-%m-%dT%H:%M:%SZ'))})
    if (category):
        dictionary.update({'Category': category})
    if (description):
        dictionary.update({'Description': description})
    if (bytes_number):
        dictionary.update({'ByteCount': bytes_number})
    if (packages):
        dictionary.update({'PacketCount': packages})

def getIdeaLog(log, anonymised, anonymised_net, aname):
    dictionary = {}
    words = log.split()
    log_type = words[4][0:-1]
    if (log_type=='RT_IDP'):
        routers = get_routers(words)
        severity = get_severity(words)
        event_id = str(uuid.uuid4())
        node_names = words[3:5]
        detect_time = timestamp_to_data(words)
        attack_sources, description = get_attack_sources(words)
        protocol_name = get_protocol(description)
        bytes_number = get_bytes_number(words)
        packages = get_packets_number(words)
        write_common_information(dictionary = dictionary,
                                 event_id = event_id,
                                 detect_time = detect_time,
                                 category= ['Other.Info'],
                                 description = description,
                                 bytes_number = bytes_number,
                                 packages = packages)
        write_sources_targets(dictionary,
                              attack_sources,
                              anonymised,
                              anonymised_net,
                              routers,
                              protocol_name)
        write_node(dictionary,
                   node_names,
                   aname)
    if (log_type=='RT_FLOW'):
        event_id = str(uuid.uuid4())
        node_names = words[3:5]
        attack_sources = [word for word in words if ('->') in word][0]
        description = ' '.join(words[words.index('session'): words.index(attack_sources)])
        date = datetime.datetime.strptime(' '.join(words[0:3]), '%b %d %H:%M:%S')
        date = date.replace(year = datetime.datetime.now().year)
        detect_time = str(date.strftime('%Y-%m-%dT%H:%M:%SZ'))
        protocol_name = words[words.index(attack_sources)+1]
        if (protocol_name == 'None'):
            protocol_name = None
        write_common_information(dictionary = dictionary,
                                 event_id = event_id,
                                 detect_time = detect_time,
                                 description = description,
                                 category= ['Other.Info'])
        write_sources_targets(dictionary,
                              attack_sources,
                              anonymised,
                              anonymised_net,
                              protocol_name=protocol_name)
        write_node(dictionary,
                   node_names,
                   aname)
    return dictionary
    
def write_sources_targets(dictionary,
                          attack_sources,
                          anonymised,
                          anonymised_net,
                          routers = None,
                          protocol_name = None,
                          ):
    sources = str(attack_sources).split("->")
    source_dictionary = {}
    if ":" in sources[0]:
        ip = 'IP6'
    else:
        ip = 'IP4'
    if anonymised != 'omit':
        if anonymised == 'yes':
            source_dictionary.update({'Anonymised':True})
            source_dictionary.update({ip: [anonymised_net.split("/")[0]]})
            source_dictionary.update({'Port': [int(anonymised_net.split("/")[1])]})
        else:
            source_dictionary.update({ip: [sources[0].split("/")[0]]})
            source_dictionary.update({'Port': [int(sources[0].split("/")[1])]})
    if (routers):
        source_dictionary.update({'Router': routers[0]})
    if (protocol_name):
        source_dictionary.update({'Proto': [protocol_name]})
    dictionary.update({'Source': [source_dictionary]})

    target_dictionary = {}
    if ":" in sources[0]:
        ip = 'IP6'
    else:
        ip = 'IP4'
    if anonymised != 'omit':
        if anonymised == 'yes':
            target_dictionary.update({'Anonymised':True})
            target_dictionary.update({ip: [anonymised_net.split("/")[0]]})
            target_dictionary.update({'Port': [int(anonymised_net.split("/")[1])]})
        else:
            target_dictionary.update({ip: [sources[1].split("/")[0]]})
            target_dictionary.update({'Port': [int(sources[1].split("/")[1])]})

    if (routers):
        target_dictionary.update({'Router': routers[1]})
    if (protocol_name):
        target_dictionary.update({'Proto': [protocol_name]})
    dictionary.update({'Target': [target_dictionary]})

def write_node(dictionary, node_names, aname):
    node_dictionary = {}
    node_dictionary.update({'Name': aname})
    node_dictionary.update({'SW': [node_names[0], node_names[1][:-1]]})
    node_dictionary.update({'Type': ["Juniper_SRX"]})
    dictionary.update({'Node': [node_dictionary]})


def get_routers(words):
    interfaces = [word for word in words if word.startswith('intf:')]
    interfaces = interfaces[0][5:-1].split('->')
    ret = []
    for a in interfaces:
        ret.append(a.split(':'))
    return ret


def get_severity(words):
    severity = [word for word in words if word.startswith('threat-severity=')]
    ret = str('severity.' + severity[0][:-1].split('=')[1].lower())
    return [ret]


def timestamp_to_data(words):
    k = words.index("At")
    timestamp = words[k+1][:-1]
    return str(datetime.datetime.fromtimestamp(int(timestamp)).strftime('%Y-%m-%dT%H:%M:%SZ'))


def get_attack_sources(words):
    index = [l for l, val in enumerate(words) if val.startswith('<')]
    attack_sources = words[index[0]][1:-1]
    k = words.index("attack:")
    description = ' '.join(words[index[0] + 1:k])
    return attack_sources, description


def create_output_name(path, i):
    return path + 'out' + str(i)


def get_bytes_number(words):
    inbytes = [word for word in words if word.startswith('inbytes=')]
    outbytes = [word for word in words if word.startswith('outbytes=')]
    bytes_number = int(inbytes[0][:-1].split('=')[1]) + int(outbytes[0][:-1].split('=')[1])
    return bytes_number


def get_packets_number(words):
    inpackets = [word for word in words if word.startswith('inpackets=')]
    outpackets = [word for word in words if word.startswith('outpackets=')]
    packages = int(inpackets[0][:-1].split('=')[1]) + int(outpackets[0][:-1].split('=')[1])
    return packages
def get_protocol(description):
    description = description.split(' ')
    k = description.index('protocol')
    return description[k - 1]
def get_last_log(logfile):
    line = subprocess.check_output(['tail', '-1', logfile])[0:-1]
    return line
def parseFile(logfile, aanonymised, atargetnet, aname):
    flag = 0
    last_log_time = get_last_log(logfile).split()[0:3]
    first_log_time = open(logfile, 'r').readline().split()[0:3]
    last_script_run = []
    if os.path.exists(last_run_file):
        with open(last_run_file, 'r') as f:
            last_script_run = f.readline().split()
    else:
        open(last_run_file,"a+")
    ideaLogs = []
    if last_log_time != last_script_run:
        if not last_script_run or datetime.datetime.strptime(' '.join(first_log_time),'%b %d %H:%M:%S')>datetime.datetime.strptime(' '.join(last_script_run),'%b %d %H:%M:%S'):
            flag = 1
        flagPassing = 0
        with open(logfile, 'r') as file:
            for line in file:
                if (flag ==1):
                    log = getIdeaLog(line,  aanonymised, atargetnet, aname)
                    if log:
                        ideaLogs.append(log)
                else:
                    if line.split()[0:3] == last_script_run and flagPassing == 0:
                        flagPassing = 1
                    elif line.split()[0:3] != last_script_run and flagPassing == 1:
                        flag =1
                        log = getIdeaLog(line,  aanonymised, atargetnet, aname)
                        if log:
                            ideaLogs.append(log)
    file = open(last_run_file, 'w')
    file.write(' '.join(last_log_time))
    return ideaLogs
    
if __name__ == "__main__":
    aconfig = read_cfg(DEFAULT_ACONFIG)
    wconfig = read_cfg(aconfig.get('warden', DEFAULT_WCONFIG))
    
    aname = aconfig.get('name', DEFAULT_NAME)
    awin = aconfig.get('awin', DEFAULT_AWIN) * 60
    wconfig['name'] = aname

    asecret = aconfig.get('secret', DEFAULT_SECRET)
    if asecret:
        wconfig['secret'] = asecret
    wclient = Client(**wconfig)

    aanonymised = aconfig.get('anonymised', DEFAULT_ANONYMISED)
    if aanonymised not in ['no', 'yes', 'omit']:
        wclient.logger.error("Configuration error: anonymised: '%s' - possible typo? use 'no', 'yes' or 'omit'" % aanonymised)
        sys.exit(2)

    anonymised_net  = aconfig.get('target_net', DEFAULT_ANONYMISED_NET)
    aanonymised = aanonymised if (anonymised_net != DEFAULT_ANONYMISED_NET) or (aanonymised == 'omit') else DEFAULT_ANONYMISED

    events = parseFile(logfile, aanonymised, anonymised_net, aname)
    print "=== Sending ==="
    start = time()
    ret = wclient.sendEvents(events)
    if ret:
        wclient.logger.info("%d event(s) successfully delivered." % len(events))
    print "Time: %f" % (time() - start)
