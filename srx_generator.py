#!/usr/bin/env python
#       File name: srx_generator.py
#       Author: Jakub Toczek (PSNC)
#       Date created: 20/11/2017
#       Python Version: 2.7.13
from warden_client import read_cfg
import datetime
from time import sleep
import random

DEFAULT_CONFIG = './generator.cfg'
DEFAULT_OUTPUT = './logs.log'
DEFAULT_MAX_PER_TIMESTAMP = 1
DEFAULT_MIN_SLEEP = 0
DEFAULT_MAX_SLEEP = 3
DEFAULT_MAX_LOGS = 10
DEFAULT_SLEEP = 1

def server():

    config = read_cfg(DEFAULT_CONFIG)
    output = config.get('output',DEFAULT_OUTPUT)
    file = open(output, "a+", 1)
    regular_log = config.get('regular')
    anomaly_log = config.get('anomaly')
    real_sleep = config.get('real_sleep',DEFAULT_SLEEP)
    min_sleep = config.get('min_sleep',DEFAULT_MIN_SLEEP)
    max_sleep = config.get('max_sleep',DEFAULT_MAX_SLEEP)
    max_logs = config.get('max_logs',DEFAULT_MAX_LOGS)
    log_count=0
    iterate = True
    date = datetime.datetime.now()
    while (iterate):
        # sleep
        if (real_sleep):
            sleep(random.uniform(min_sleep, max_sleep))
            date = datetime.datetime.now()
        else:
            sleep_time = random.uniform(min_sleep, max_sleep)
            date = date+datetime.timedelta(seconds=sleep_time)
        if (random.randint(1,100) != 1):
            file.write(create_srx_log(regular_log, date))
        else:
            file.write(create_srx_log(anomaly_log, date))
        log_count+=1
        if log_count>=max_logs:
            iterate=False


def create_srx_log(log_type, date):
    header = date.strftime('%b %d %H:%M:%S') + " " + random.choice(log_type.get('node_name')) + " " + random.choice(log_type.get('sw')) + ": " + random.choice(log_type.get('description2')) + ": " + "Lsys " + random.choice(log_type.get("Lsys")) + ": IDP: At " + str(int((date).strftime('%s')) + 1) + ", SIG Attack log <" + random.choice(log_type.get('src')) + "->" + random.choice(log_type.get('target')) + "> for " + random.choice(log_type.get('protocol')) + " " + random.choice(log_type.get('description')) + " "

    body = []
    body.append("id="+str(random.randint(10000,99999)))
    body.append(getKeyRValue(log_type, "repeat", "="))
    body.append(getKeyRValue(log_type, "action", "="))
    body.append(getKeyRValue(log_type, "threat-severity", "="))
    body.append(getKeyRValue(log_type, "name", "="))
    body.append("NAT " + str(random.choice(log_type.get("nat"))))
    body.append(getKeyRValue(log_type, "time-elapsed", "="))
    body.append(getKeyRValue(log_type, "inbytes", "="))
    body.append(getKeyRValue(log_type, "outbytes", "="))
    body.append(getKeyRValue(log_type, "inpackets", "="))
    body.append(getKeyRValue(log_type, "outpackets", "="))
    body.append(getKeyRValue(log_type, "intf", ":"))
    body.append(getKeyRValue(log_type, "packet-log-id", ":"))
    body.append(getKeyRValue(log_type, "alert", "="))
    body.append(getKeyRValue(log_type, "username", "="))
    body.append(getKeyRValue(log_type, "roles", "="))

    full_body = "attack: "+', '.join(body)
    ending = " and misc-message -"

    return header+full_body+ending+"\n"

def getKeyRValue(dictionary, key, delimiter):
    return key+delimiter+str(random.choice(dictionary.get(key)))

server()